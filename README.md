# Hypos

![Hypotrochoids being drawn](/movies/hypos.gif)

Draws up to 10 random [Hypotrochoids](http://mathworld.wolfram.com/Hypotrochoid.html) on screen
at random intervals.

## Usage

Open hypos.pde on Processing and run.
