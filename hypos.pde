import java.util.List;
import java.util.LinkedList;

class Hypotrochoid {
  int R;
  int r;
  float theta;
  int d;

  int x;
  int y;  

  PGraphics buf;

  Hypotrochoid(int x, int y, int R, int r, int d) {
    this.x = x;
    this.y = y;
    this.R = R;
    this.r = r;

    this.theta = random(2*3.14);
    this.d = d;

    int w = max(2*R, 2*(d+r+R));
    this.buf = createGraphics(w, w);
  }

  void drawOuter() {
    ellipse(x, y, 2*R, 2*R);
  }

  void drawInner() {
    int x_c = round((R - r)*cos(theta));
    int y_c = round((R - r)*sin(theta));
    ellipse(x + x_c, y + y_c, 2*r, 2*r);
  }

  void drawBar() {
    int x_c = round((R - r)*cos(theta));
    int y_c = round((R - r)*sin(theta));

    int x_i = round((R - r)*cos(theta) + d*cos(theta*(R - r)/r));
    int y_i = round((R - r)*sin(theta) - d*sin(theta*(R - r)/r));

    line(x + x_c, y + y_c, x + x_i, y + y_i);
  }

  void draw() {
    int x_i = round((R - r)*cos(theta) + d*cos(theta*(R - r)/r));
    int y_i = round((R - r)*sin(theta) - d*sin(theta*(R - r)/r));
    drawOuter();
    drawInner();
    drawBar();
    buf.beginDraw();
    buf.stroke(108, 146, 159);
    buf.point(x_i+buf.width/2, y_i+buf.height/2);
    buf.endDraw();
    image(buf, x - buf.width/2, y - buf.height/2);
  }

  void tick() {
    theta = theta + ((float)r)/R/(frameRate/2);

    if (theta < 0) theta = 0;
  }
}

List<Hypotrochoid> hypos = new LinkedList<Hypotrochoid>();

void setup() {
  size(400, 400); 
  fill(0, 0);
  background(0);
  stroke(108, 146, 159);
  frameRate(200);
}

int randInt(int x) {
  return (int) random(x);
}

void draw() {
  if (random(1000) > 999 && hypos.size() < 10) {
    int bigRadius = randInt(150) + 1;
    hypos.add(new Hypotrochoid(randInt(350), randInt(350), bigRadius, randInt(bigRadius - 1) + 1, randInt(150) + 1));
  }
  background(0);
  for (Hypotrochoid h : hypos) {
    h.draw();
    h.tick();
  }
}